# Ethereum Privacy - Web site repository

This site is built using polymer 3.0 components.

It is hosted, for now in gitlab pages : https://guenoledc-perso.gitlab.io/offchainprivatetx/website/

It is fully static, and interact with 2 remote APIs
- Etherscan.io to collect public blockchain info (to prove that the Privacy node talk to the rea network ;-)
- Privacy node, currently hosted in Azure (hosting credit needed)


## Thanks to
- All the Ethereum community from which I have collected a lots of knowledge (stackoverflow, Frozeman, and many others)
- Gitlab.com for the great tools they provide and giving me a free service
- My kids, for their understanding of my time spent developping
- ... and many others



![Logo](./images/icon/apple-icon-180x180.png "Ethereum Privacy")