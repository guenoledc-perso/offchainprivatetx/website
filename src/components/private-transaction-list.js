import '@polymer/iron-ajax';
//import { html2, Polymer } from "@polymer/polymer/polymer-legacy.js";
import { html } from '@polymer/polymer/lib/utils/html-tag.js';
import { PolymerElement } from '@polymer/polymer/polymer-element.js';
import '@vaadin/vaadin-grid/vaadin-grid.js';
import '../shared-styles.js';
import './new-tab-link';




window.addEventListener('WebComponentsReady', function() {
class PrivateTransactionList extends PolymerElement {
  static get template() {
    return html`
    <web3-json-rpc id="jsonrpc" hidden result="{{_lastBlockDetails}}" node-url="{{privateUrlNode}}" method="eth_getBlockByNumber" private></web3-json-rpc>
    <web3-json-rpc id="txReceipt" hidden result="{{_TxReceipt}}" node-url="{{privateUrlNode}}" method="eth_getTransactionReceipt" private></web3-json-rpc>
    <vaadin-grid theme="row-dividers" column-reordering-allowed multi-sort items={{items}}  style="height:200px">
  <style>
      :host {
        font-size: x-small
      }
      [part="header-cell"] {
        font-weight: 900
      }
  </style>
  <vaadin-grid-column  width="20px" resizable> 
      <template class="header">Block# - index</template>
      <template>[[item.blockNumber]] - [[item.transactionIndex]]</template>
    </vaadin-grid-column>
    <vaadin-grid-column header="Tx Hash" path="hash" width="9em" resizable>
      <template class="header">Tx Hash (click to see details)</template>
      <template><new-tab-link on-click="_onClick" display="[[item.hash]]"></new-tab-link></template>
    </vaadin-grid-column> 
  </vaadin-grid>

`;
  }

  static get is() {return 'private-transaction-list'}
  /**
   * @event on-click
   */

  static get properties() {
    return {
      privateUrlNode: {type: String, notify:true},
      items : {type: Array, notify:true, value:[] },
      lastError: String,
      _lastBlockDetails: {type: Object, observer:'_newBlockResponse'},
      _TxReceipt: {type: Object, observer:'_txReceiptResponse'},
      _loadedBlocks: {type: Object, value:{} }
    }
  }
  
  _onClick(e) {
    //console.log('On Click raised', e)
    e.selected = this.items.find( t=>t.hash == e.target.display )
    if(!e.selected.receipt) {
      this.$.txReceipt.params=[e.selected.hash]
      this.$.txReceipt.triggerRequest()
    } 
    this.dispatchEvent(new CustomEvent('on-click', {
      bubbles: true,
      composed: true,
      event: e
    }));
  }

  _txReceiptResponse(receipt) {
    let tx = this.items.find( t=>t.hash == receipt.transactionHash )
    tx.receipt = receipt
  }

  _newBlockResponse() {
    //console.log('newBlockResponse', this._lastBlockDetails)
    if(this._lastBlockDetails == null) ;
    else if(typeof this._lastBlockDetails == 'string') // we have an error
      this._lastError = this._lastBlockDetails
    else {
      let blockNumber = +this._lastBlockDetails.number
      this._lastBlockDetails.transactions.forEach( t=> {t.blockNumber = +t.blockNumber; t.transactionIndex = +t.transactionIndex})
      this._loadedBlocks[blockNumber] = this._lastBlockDetails

      this.items = Object.values(this._loadedBlocks).sort( (a,b)=> (+b.number-a.number) ).flatMap( b=>b.transactions )
      //this.items = this.items.concat(this._lastBlockDetails.transactions)
      this.$.jsonrpc.params = [blockNumber+1, true]
      this.$.jsonrpc.triggerRequest()
      //console.log('Trigger private tx transaction requests')

    }
  }
  loadAllTransactions(full) {
    let lastBlockLoaded
    if(!full) {
      lastBlockLoaded = Object.keys(this._loadedBlocks).sort().reverse()[0]
      if(!lastBlockLoaded) lastBlockLoaded=0 
      else lastBlockLoaded++
    } else {
      this.items=[]
      this._loadedBlocks={}
      lastBlockLoaded=0
    }
    this.$.jsonrpc.params = [lastBlockLoaded, true]
    this.$.jsonrpc.triggerRequest()
  }

  ready() {
    super.ready()
    //let grid = this.shadowRoot.querySelector('vaadin-grid')
    //grid.dataProvider = 
    let self = this
    setTimeout( ()=> {
        //this.$.jsonrpc.url_node = this.privateUrlNode
        self.loadAllTransactions()
        //console.log('Trigger private tx transaction requests')
    }, 200 )
    
  }
}
window.customElements.define(PrivateTransactionList.is, PrivateTransactionList);

})
