import { PolymerElement } from '@polymer/polymer/polymer-element.js';
import { declareComponent } from '../lib/util';


import '../shared-styles.js';
import '@polymer/iron-ajax'

import '@vaadin/vaadin-grid/vaadin-grid.js';
import './new-tab-link'

class EtherscanTransactionList extends PolymerElement {

  static get is() {return 'etherscan-transaction-list'}
  /**
   * @event on-click
   */

  static get properties() {
    return {
      lastBlockNumber: {type:String},
      _lastResponse: {type:Object, observer:'lastResponseChanged'},
      _queryUrl: {type:String, computed:'_buildQueryUrl(lastBlockNumber)'},
      _items: {type: Array, value:[]}
    }
  }
  
  lastResponseChanged(lastResponse) {
    if(!lastResponse.result) return
    let newItems = []
    lastResponse.result.reverse().forEach( tx=> newItems.unshift(tx))
    this._items.forEach( tx=> newItems.push(tx))
    this._items = newItems
  }

  _buildQueryUrl(lastBlockNumber) {
    let startBlock = 0
    if(this._items.length>0) startBlock = new String(+this._items[0].blockNumber+1).toString()
    let url = `${config.etherscan_api_root}/api?module=account&action=txlist&address=${config.private_tx_smart_contract}&startblock=${startBlock}&endblock=${lastBlockNumber}&page=1&offset=30&sort=desc&apikey=YourApiKeyToken`
    //console.log('building query url', url)
    return url
  }

  ready() {
    super.ready()
    //console.log("Ready")
  }
}

window.addEventListener('WebComponentsReady', function() {
  declareComponent(EtherscanTransactionList)
})
