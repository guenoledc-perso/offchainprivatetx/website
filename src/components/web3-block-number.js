import { PolymerElement } from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '@polymer/paper-button/paper-button.js';
import '../shared-styles.js';
import { html } from '@polymer/polymer/lib/utils/html-tag.js';
class Web3BlockNumber extends PolymerElement {
  static get template() {
    return html`
    <iron-ajax id="getBlockNumber" method="get" handle-as="json" last-response="{{eth_blockNumber}}" on-response="handleNewResponse" on-error="handleError">
    </iron-ajax>
    [[blockNumber]]
`;
  }

  static get is() {return 'web3-block-number'}
  /**
   * @event web3-block-number-changed
   */
  static get properties() {
    return {
      eth_blockNumber: Object,
      blockNumber: {
        type:Number,
        notify: true,
        computed: 'formatBlockNumber(eth_blockNumber)'
      },
      apiKey: String,
      _lastError: String,
      _lastBlockNumber: Number
    }
  }
  handleError() {
    this._lastError = 'Connection lost!'
    this.eth_blockNumber={} // force a refresh (Its not a goot approach - rethink)
    //console.log(this.eth_blockNumber)
    setTimeout( this.triggerRequest.bind(this), 5000 ) // longer delay
  }
  handleNewResponse(response) {
    //console.log(this.eth_blockNumber)
    
    if(this._lastBlockNumber!=this.blockNumber) {
      this._lastBlockNumber=this.blockNumber
      this.dispatchEvent(new CustomEvent('web3-block-number-changed', {
        bubbles: true,
        composed: true
      }));
    }
    this._lastError = null
    setTimeout( this.triggerRequest.bind(this), 2000 )
  }

  // Element class can define custom element reactions
  connectedCallback() {
    super.connectedCallback();
    //console.log('element created!');
  }

  ready() {
    super.ready();
    this.$.getBlockNumber.url=`${config.etherscan_api_root}/api?module=proxy&action=eth_blockNumber&apikey=${this.apiKey}`
    this.triggerRequest()
    //this.textContent = 'loading..';
  }
  formatBlockNumber(eth_blockNumber) {
    if(this._lastError || !eth_blockNumber) return this._lastError
    return +eth_blockNumber.result
  }
  triggerRequest() {
    this.$.getBlockNumber.generateRequest();
  }
}
window.customElements.define(Web3BlockNumber.is, Web3BlockNumber);
