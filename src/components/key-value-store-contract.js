import { PolymerElement } from '@polymer/polymer/polymer-element.js';
import { html } from '@polymer/polymer/lib/utils/html-tag.js';

import { web3Instance } from '../lib/util'
import '../shared-styles.js';


class KeyValueStoreContract extends PolymerElement {
  static get template() {
    return html`
      <div>must be hidden</div>
    `;
  }

  static get is() {return 'key-value-store-contract'}
  /**
   * @event on-start-transaction
   */
  /**
   * @event on-complete-transaction
   */
  /**
   * @event on-new-event
   */

  static get properties() {
    return {
      url: {type:String, observer:'_urlChanged'},
      selectedNodes: {type:Array},
      address: {type:String, notify:true, observer:'_addressChanged'}, // the address of the smart contract, once created
      account: String, // the account to use to submit transaction to the privacy node
      eventFilter: {type:Object, value:null},
      lastEvent: {type:Object, value:"", notify:true}, // changes will happen as often as a new event is polled
    }
  }
  
  ready() {
    super.ready()
    this.setAttribute('hidden', true)
  }

  startTransaction(action, request, hash) {
    this.dispatchEvent(new CustomEvent('start-transaction', {
      bubbles: true,
      composed: true,
      detail: {action: action, request: request, hash:hash}
    }));
  }
  endTransaction(action, hash, response) {
    this.dispatchEvent(new CustomEvent('complete-transaction', {
      bubbles: true,
      composed: true,
      detail: {action: action, response: response, hash:hash}
    }));
  }
  fireNewEvent(event) {
    this.lastEvent = event.args
    this.dispatchEvent(new CustomEvent('new-event', {
      bubbles: true,
      composed: true,
      detail: event
    }));
  }

  getContract() { 
    return web3Instance(this.url, this.account).eth.contract([{"constant":true,"inputs":[{"name":"key","type":"string"}],"name":"getValue","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"key","type":"string"},{"name":"value","type":"string"}],"name":"setValue","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"anonymous":false,"inputs":[{"indexed":false,"name":"key","type":"string"},{"indexed":false,"name":"owner","type":"address"},{"indexed":false,"name":"created","type":"bool"}],"name":"newKeySet","type":"event"}]);
  }
  static get newTx() {
    return {
      from: '', // To be replaced at execution time 
      data: '0x608060405234801561001057600080fd5b50610a88806100206000396000f3fe608060405260043610610046576000357c010000000000000000000000000000000000000000000000000000000090048063960384a01461004b578063ec86cfad1461018c575b600080fd5b34801561005757600080fd5b506101116004803603602081101561006e57600080fd5b810190808035906020019064010000000081111561008b57600080fd5b82018360208201111561009d57600080fd5b803590602001918460018302840111640100000000831117156100bf57600080fd5b91908080601f016020809104026020016040519081016040528093929190818152602001838380828437600081840152601f19601f8201169050808301925050505050505091929192905050506102eb565b6040518080602001828103825283818151815260200191508051906020019080838360005b83811015610151578082015181840152602081019050610136565b50505050905090810190601f16801561017e5780820380516001836020036101000a031916815260200191505b509250505060405180910390f35b34801561019857600080fd5b506102e9600480360360408110156101af57600080fd5b81019080803590602001906401000000008111156101cc57600080fd5b8201836020820111156101de57600080fd5b8035906020019184600183028401116401000000008311171561020057600080fd5b91908080601f016020809104026020016040519081016040528093929190818152602001838380828437600081840152601f19601f8201169050808301925050505050505091929192908035906020019064010000000081111561026357600080fd5b82018360208201111561027557600080fd5b8035906020019184600183028401116401000000008311171561029757600080fd5b91908080601f016020809104026020016040519081016040528093929190818152602001838380828437600081840152601f19601f8201169050808301925050505050505091929192905050506103fb565b005b60606000826040518082805190602001908083835b6020831015156103255780518252602082019150602081019050602083039250610300565b6001836020036101000a03801982511681845116808217855250505050505090500191505090815260200160405180910390206001018054600181600116156101000203166002900480601f0160208091040260200160405190810160405280929190818152602001828054600181600116156101000203166002900480156103ef5780601f106103c4576101008083540402835291602001916103ef565b820191906000526020600020905b8154815290600101906020018083116103d257829003601f168201915b50505050509050919050565b600073ffffffffffffffffffffffffffffffffffffffff166000836040518082805190602001908083835b60208310151561044b5780518252602082019150602081019050602083039250610426565b6001836020036101000a038019825116818451168082178552505050505050905001915050908152602001604051809103902060000160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16141561069d577f43593f29d04f04eeab1f0a690c4e970d514b01584d11db79cf4590275917e2e98233600160405180806020018473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200183151515158152602001828103825285818151815260200191508051906020019080838360005b8381101561055f578082015181840152602081019050610544565b50505050905090810190601f16801561058c5780820380516001836020036101000a031916815260200191505b5094505050505060405180910390a160408051908101604052803373ffffffffffffffffffffffffffffffffffffffff168152602001828152506000836040518082805190602001908083835b6020831015156105fe57805182526020820191506020810190506020830392506105d9565b6001836020036101000a038019825116818451168082178552505050505050905001915050908152602001604051809103902060008201518160000160006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff1602179055506020820151816001019080519060200190610694929190610937565b50905050610933565b3373ffffffffffffffffffffffffffffffffffffffff166000836040518082805190602001908083835b6020831015156106ec57805182526020820191506020810190506020830392506106c7565b6001836020036101000a038019825116818451168082178552505050505050905001915050908152602001604051809103902060000160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1614156108c4577f43593f29d04f04eeab1f0a690c4e970d514b01584d11db79cf4590275917e2e98233600060405180806020018473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200183151515158152602001828103825285818151815260200191508051906020019080838360005b838110156108005780820151818401526020810190506107e5565b50505050905090810190601f16801561082d5780820380516001836020036101000a031916815260200191505b5094505050505060405180910390a1806000836040518082805190602001908083835b6020831015156108755780518252602082019150602081019050602083039250610850565b6001836020036101000a038019825116818451168082178552505050505050905001915050908152602001604051809103902060010190805190602001906108be9291906109b7565b50610932565b6040517f08c379a000000000000000000000000000000000000000000000000000000000815260040180806020018281038252600b8152602001807f4e6f7420616c6c6f77656400000000000000000000000000000000000000000081525060200191505060405180910390fd5b5b5050565b828054600181600116156101000203166002900490600052602060002090601f016020900481019282601f1061097857805160ff19168380011785556109a6565b828001600101855582156109a6579182015b828111156109a557825182559160200191906001019061098a565b5b5090506109b39190610a37565b5090565b828054600181600116156101000203166002900490600052602060002090601f016020900481019282601f106109f857805160ff1916838001178555610a26565b82800160010185558215610a26579182015b82811115610a25578251825591602001919060010190610a0a565b5b509050610a339190610a37565b5090565b610a5991905b80821115610a55576000816000905550600101610a3d565b5090565b9056fea165627a7a72305820e94109ebfbfde689e9d6a541f66053bd17e8de88cfe4c5192f3f81f3f50aa8f20029', 
      gas: '800000'
    }
  }
  
  pushParkTransactionIfAny(hash) {
    web3Instance(this.url).currentProvider.sendAsync({
      jsonrpc:"2.0", id:1, 
      method:"parked_sendTransaction", 
      params:[hash, {privateFor:this.selectedNodes}]
    }, console.log)
  }

  createNewInstance() {
    let tx = KeyValueStoreContract.newTx
    tx.from = this.account
    tx.privateFor=this.selectedNodes
    let self=this
    self.address = '' // erase previous address since it will be replaced
    
    this.getContract().new( tx , function (e, contract){
        if(e) console.error(e)
        else console.log(contract);
        if(e) return
        if (typeof contract.address !== 'undefined') {
            console.log('Contract mined! address: ' + contract.address + ' transactionHash: ' + contract.transactionHash);
            self.address = contract.address
            self.endTransaction('new-contract',contract.transactionHash, contract)
        } else if( contract.transactionHash ) {
          self.startTransaction('new-contract', tx, contract.transactionHash)
          self.pushParkTransactionIfAny(contract.transactionHash)
        }
    })
  }

  _stopWatchingChanges() {
    if(this.eventFilter) { 
      try{this.eventFilter.stopWatching()}catch(e) {}
    }
    this.eventFilter=null
  }

  _urlChanged(url){
    console.log('New url', url)
    this._startWatchingChanges()
  }
  _addressChanged(address) {
    console.log('New address', address)
    this._startWatchingChanges()
  }

  _startWatchingChanges() {
    this._stopWatchingChanges()
    if(!this.address.match(/^0x[0-9a-fA-F]{40}$/)) return
    let instance = this.getContract().at(this.address)
    this.eventFilter = instance.newKeySet({}, {fromBlock:0, toBlock:'latest'});
    console.log('CREATE NEW FILTER', !!this.eventFilter)
    if(!this.eventFilter) return
    let self = this
    try {
      this.eventFilter.watch( (error, event)=>{
        if(error) {
          console.error('Error when checking for events for contract '+self.address, error)
          setTimeout( self._startWatchingChanges.bind(self) , 500)
        } else {
          //console.log('KeyPair event received', event)
          self.collectValueInEvent(event, (event)=>{
            self.fireNewEvent(event)
          })
        }
      })
    } catch (failWatching) {
      setTimeout( self._startWatchingChanges.bind(self) , 1000)
    }
  }

  collectValueInEvent( event, cb ) {
    if(!event) return cb(null)
    if(!event.args) return cb(null)
    if(!event.args.key || !event.args.owner) return cb(null)
    let block = event.blockNumber || 'latest'

    if(!this.address.match(/^0x[0-9a-fA-F]{40}$/)) return
    let instance = this.getContract().at(this.address)
    
    instance.getValue(event.args.key, {from:this.account}, block, (err, value)=>{
      if(!err && value) event.args.value = value
      cb(event) 
    })
  }

  setKeyValue(key, value) {
    if(!this.address.match(/^0x[0-9a-fA-F]{40}$/)) return
    let instance = this.getContract().at(this.address)
    let self = this
    instance.setValue(key, value, {from:this.account, gas:100000, privateFor:this.selectedNodes}, (err, hash)=>{
      console.log('Submitting new KeyPair', key, value, err, hash)
      self.startTransaction('set-keypair', {key:key, value:value, owner:self.account}, hash)
      self._waitForTransaction(hash, (err, receipt)=>{
        self.endTransaction('set-keypair', hash, receipt)
      })
      self.pushParkTransactionIfAny(hash)
    })
  }

  _waitForTransaction(hash, cb) {
    if(!this.address.match(/^0x[0-9a-fA-F]{40}$/)) return
    let eth = web3Instance(this.url, this.account).eth
    let filter = eth.filter('latest', (err, blockHash)=>{
      if(err || !blockHash) return
      eth.getTransactionReceipt(hash, (err, receipt)=>{
        if(err) return
        if(receipt) {
          try{eth.filter.stopWatching()}catch(e){}
          cb(null, receipt)
        }
        // else wait next block
      })
    })
    let instance = this.getContract().at(this.address)

  }

}


window.addEventListener('WebComponentsReady', function() {
  window.customElements.define(KeyValueStoreContract.is, KeyValueStoreContract);
})