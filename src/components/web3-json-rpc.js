import { PolymerElement } from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '@polymer/paper-button/paper-button.js';
import '../shared-styles.js';
import { html } from '@polymer/polymer/lib/utils/html-tag.js';
let globalRequestCounter=1
export class Web3JsonRpc extends PolymerElement {
  static get template() {
    return html`
<iron-ajax id="jsonRpcQuery" auto="{{auto}}" url="{{nodeUrl}}" method="post" body="[[rpcRequest]]" handle-as="json" last-response="{{rpcResponse}}" on-response="handleResponse" on-error="handleError">
    </iron-ajax>[[resultAsText]]
`;
  }

  static get is() {return 'web3-json-rpc'}
  static get counter() {return globalRequestCounter++}
  /**
   * @event on-result
   */
  static get properties() {
    return {
      lastId: {type: Number, value: globalRequestCounter++},
      nodeUrl: String,
      auto: {type:Boolean, value:false},
      rpcResponse: Object,
      rpcRequest: {
        type:Object,
        computed: 'getRequest(lastId, method, params, private, public)'
      },
      method: String,
      params: {
        type:Object, value:[]
      },
      result: {
        type:String,
        notify: true,
        computed: 'getResult(rpcResponse)'
      },
      resultAsText: {
        type:String,
        notify: true,
        computed: 'getResultAsText(rpcResponse)'
      },
      _lastError: String,
      private: {
        type: Boolean,
        value:false
      },
      public:  {
        type: Boolean,
        value:false
      },
      autoRefresh: {
        type:Number,
        value:0
      }
    }
  }
  handleError() {
    this._lastError = 'technical error'
    this.rpcResponse={jsonrpc:"2.0", error:this._lastError} // force a refresh (Its not a goot approach - rethink)
    if(this.autoRefresh>0 && this.autoRefresh < 100) this.autoRefresh=100
    if(this.autoRefresh>0) setTimeout( this.triggerRequest.bind(this), this.autoRefresh )
  }
  handleResponse() {
    //console.log(this.rpcResponse)
    if(this.autoRefresh>0 && this.autoRefresh < 100) this.autoRefresh=100
    if(this.autoRefresh>0) setTimeout( this.triggerRequest.bind(this), this.autoRefresh )
    this.dispatchEvent(new CustomEvent('result', {
      bubbles: true,
      composed: true,
      detail: this.result
    }));
  }
  getRequest(lastId, method, params, _private, _public) {
    //console.log("getRequest",lastId, method, params, _private, _public)
    let _params = Array.isArray(params)? params.slice() : params.split(',')
    if(_private) _params.push({private:true}) // private make public being ignored
    if(!_private && _public) _params.push({private:false})
    let req = {jsonrpc:"2.0", id:globalRequestCounter++, method:method, params:_params}
    //console.log('Sending request:'+JSON.stringify(req))
    return JSON.stringify(req)
  }
  getResult(rpcResponse) {
    let ret
    if(this._lastError || !rpcResponse) ret = this._lastError
    else if(rpcResponse.error && rpcResponse.error.message) ret = rpcResponse.error.message
    else if(rpcResponse.result) ret = rpcResponse.result
    else ret = 'invalid rpc response'
    //console.log('Web3Response', ret)
    return ret
  }
  getResultAsText(rpcResponse) {
    return JSON.stringify(this.getResult(rpcResponse), null, 2)
  }

  triggerRequest() {
    this._lastError=null
    this.lastId = globalRequestCounter
    //console.log('JsonRpc Global Counter='+globalRequestCounter)
    if(!this.auto) this.$.jsonRpcQuery.generateRequest();
  }

  // Element class can define custom element reactions
  connectedCallback() {
    super.connectedCallback();
    //console.log('element created!');
  }

  ready() {
    super.ready();
    this.textContent = 'loading..';
  }
}
window.customElements.define(Web3JsonRpc.is, Web3JsonRpc);
