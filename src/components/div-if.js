import { PolymerElement } from '@polymer/polymer/polymer-element.js';
import '../shared-styles.js';
import { html } from '@polymer/polymer/lib/utils/html-tag.js';
class DivIf extends PolymerElement {
  static get template() {
    return html`
    <div id="content"><slot name="content">Default content</slot></div>
`;
  }

  static get is() {return 'div-if'}


  static get properties() {
    return {
      if: {type:Boolean, value:undefined, notify:true, observer:'_changeState'},
    }
  }
  ready() {
    super.ready()
    this._changeState()
  }
  _changeState() {
    if(this.if) this.$.content.style.display='contents'
    else this.$.content.style.display='none'
  }
}
window.customElements.define(DivIf.is, DivIf);
