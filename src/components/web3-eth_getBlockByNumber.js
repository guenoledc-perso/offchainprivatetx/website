import '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '@polymer/paper-button/paper-button.js';
import {Web3JsonRpc} from './web3-json-rpc.js';
import '../shared-styles.js';
class Web3EthGetBlockByNumber extends Web3JsonRpc {
  static get is() {return 'web3-eth-get-block-by-number'}
  static get counter() {return globalRequestCounter++}
  static get properties() {
    return {
      blockTag : {type:String, observer:'_setParams' },
      withTransaction: {type: Boolean, observer:'_setParams'}
    }
  }
  _setParams() {
    this.params = [this.blockTag, this.withTransaction?true:false]
  }
  
}
window.customElements.define(Web3EthGetBlockByNumber.is, Web3EthGetBlockByNumber);
