import '@polymer/iron-ajax';
//import { html2, Polymer } from "@polymer/polymer/polymer-legacy.js";
import { html } from '@polymer/polymer/lib/utils/html-tag.js';
import { PolymerElement } from '@polymer/polymer/polymer-element.js';
import '@vaadin/vaadin-grid/vaadin-grid.js';
import '../shared-styles.js';
import './new-tab-link';




window.addEventListener('WebComponentsReady', function() {
let rpcCounter=0
class PrivateTransactionList extends PolymerElement {
  static get template() {
    return html`
    <vaadin-grid theme="row-dividers" column-reordering-allowed multi-sort items={{_displayItems}} style="height:200px">
      <style>
          :host {
            font-size: x-small
          }
          [part="header-cell"] {
            font-weight: 900
          }
      </style>
      <vaadin-grid-column  width="20px" resizable> 
        <template class="header">Key</template>
        <template>[[item.key]]</template>
      </vaadin-grid-column>
      <vaadin-grid-column  width="20px" resizable> 
        <template class="header">Value</template>
        <template>[[item.value]]</template>
      </vaadin-grid-column>
      <vaadin-grid-column width="9em" resizable>
        <template class="header">Owner wallet</template>
        <template>[[item.owner]]</template>
      </vaadin-grid-column> 
      <vaadin-grid-column width="20px" resizable>
        <template class="header">changed at block</template>
        <template>[[item.block]]</template>
      </vaadin-grid-column> 
    </vaadin-grid>

`;
  }

  static get is() {return 'key-value-pair-list'}
  /**
   * @event on-click
   */

  static get properties() {
    return {
      address : {type: String, notify:true, observer:'_addressChanged'},
      newEvent : {type: Object, notify:true, observer:'_processNewEvent' },
      _cachedItems: {type: Object, value:{}},
      _displayItems: {type: Array, computed:'_calcDisplayItems(_cachedItems)'}
    }
  }

  _addressChanged(newAddress, oldAddress) {
    this._cachedItems = {}
  }

  _processNewEvent(newEvent, oldEvent) {
    //console.log('NewEvent:', newEvent)
    if(!newEvent) return
    let tempItems = this._cachedItems
    tempItems[newEvent.args.key] = {value:newEvent.args.value, owner:newEvent.args.owner, block:newEvent.blockNumber}
    this._cachedItems = Object.assign({}, tempItems)
  }
  _calcDisplayItems(_cachedItems) {
    return Object.keys(_cachedItems).sort().map( key=>{
      return {key:key, block:_cachedItems[key].block, value:_cachedItems[key].value, owner:_cachedItems[key].owner}
    } )
  }

  ready() {
    super.ready()

    
  }
}
window.customElements.define(PrivateTransactionList.is, PrivateTransactionList);

})
