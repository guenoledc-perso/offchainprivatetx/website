import { PolymerElement } from '@polymer/polymer/polymer-element.js';
import '../shared-styles.js';
import { html } from '@polymer/polymer/lib/utils/html-tag.js';
class NewTabLink extends PolymerElement {
  static get template() {
    return html`
    <style>
        .img {
            height: 0.8em;
        }    
    </style>
    <a id="link" href="{{href}}" target="_blank" rel="noopener noreferrer">{{display}} <img src="images/a-new-tab.png" class="img"></a>
`;
  }

  static get is() {return 'new-tab-link'}
  /**
   * @event on-click
   */

  static get properties() {
    return {
      href: String,
      display: String
    }
  }
  _onClick(e) {
    //console.log('On Click raised', e)
    this.dispatchEvent(new CustomEvent('on-click', {
      bubbles: true,
      composed: true,
      event: e
    }));
  }
  ready() {
    super.ready()
    this.$.link.addEventListener('click', this._onClick.bind(this));
  }
}
window.customElements.define(NewTabLink.is, NewTabLink);
