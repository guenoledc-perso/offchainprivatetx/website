import { html } from '@polymer/polymer/lib/utils/html-tag.js';

const notFoundTemplate = `
    <div style="color:red;">Template unavailable</div>
`;

/**
 * currentScript replaces the not backward compatible import.meta.url (after Firefox 62 ish)
 * It uses the error stack trace to find where the error is generated from
 * Polymer build  --js-transform-import-meta does not seems to work for me, so this is a work around
 * @param {*} level additional stack level to get the script from. Usefull when this call is embeded into another call
 */
export const currentScript = function(level) {
    if(!level) level=0
    //console.log('STACK', new Error().stack.split(window.location.origin))
    return window.location.origin
          +(new Error()).stack.split(window.location.origin)[2+level]
                .split(':')[0]
}

export const declareComponent = function(PolymerClass) {
    const importMetaUrl=currentScript(1)
    console.log('CURRENT SCRIPT:', importMetaUrl)
    const xhr = new XMLHttpRequest()
    xhr.onload = ()=>{
        if(xhr.status!=200) {
            console.error('Fail loading the template file for <'+PolymerClass.is+'> looked at '+xhr.responseURL)
            PolymerClass.template = html([notFoundTemplate])
        } else {
            //console.log('loading PolymerClass', meta)
            let template=xhr.responseText
            PolymerClass.template = html([eval("`"+template+"`")])
        }
        window.customElements.define(PolymerClass.is, PolymerClass);
      }
      xhr.open('GET',importMetaUrl.replace('.js','.html'), true)
      xhr.send()
}

export const web3Instance = function(url, selectedAddress) {
    if (globalThis.web3 && globalThis.ethereum // Metamask is installed
        && globalThis.ethereum.isMetaMask 
        && +globalThis.ethereum.networkVersion == globalThis.config.private_netword_id // and it is on our node
        && selectedAddress && globalThis.ethereum.selectedAddress
        && globalThis.ethereum.selectedAddress == selectedAddress) // and it is unlocked
            return globalThis.web3
    
    if(!Web3) {
        console.error('Load a web3js library that defines the Web3 class')
        return null // 
    }
    // Not using Metamask return a previously created instance
    if(globalThis.web3js) 
        return globalThis.web3js
    
    // no instance created yet: create one and return it
    globalThis.web3js = new Web3( new Web3.providers.HttpProvider(url) )
    return globalThis.web3js
}