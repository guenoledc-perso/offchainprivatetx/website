/**
@license
Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
Code distributed by Google as part of the polymer project is also
subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
*/
/*
<link rel="import" href="../bower_components/paper-dialog-scrollable/paper-dialog-scrollable.js">
*/
/*
  FIXME(polymer-modulizer): the above comments were extracted
  from HTML and may be out of place here. Review them and
  then delete this comment!
*/
import { PolymerElement } from '@polymer/polymer/polymer-element.js';

import {} from '@polymer/polymer/lib/elements/dom-repeat.js';
import {} from '@polymer/polymer/lib/elements/dom-if.js';

import '@polymer/iron-ajax/iron-ajax.js';
import '@polymer/paper-button/paper-button.js';
import '@polymer/iron-flex-layout/iron-flex-layout.js';
import '@polymer/paper-dialog/paper-dialog.js';
import '@polymer/paper-dialog-scrollable';
import '@polymer/paper-checkbox';
import '@polymer/paper-dropdown-menu/paper-dropdown-menu.js';
import '@polymer/paper-item/paper-item.js';
import '@polymer/paper-listbox/paper-listbox.js';
import '@polymer/paper-input/paper-input'
import '@polymer/paper-icon-button'

//import 'web-animations-js/web-animations-next-lite.min.js';

import './shared-styles.js';
import './components/div-if'
import './components/web3-block-number.js';
import './components/new-tab-link.js';
import './components/web3-json-rpc.js';
import './components/web3-eth_getBlockByNumber.js';
import './components/JSONFormatter.js'
import './components/etherscan-transaction-list'
import './components/private-transaction-list'
import './components/key-value-store-contract'
import './components/key-value-pair-list'

import { declareComponent, web3Instance } from "./lib/util.js";

class Content extends PolymerElement {
  static get is() { return 'my-content'; }
  static get properties() {
    return {
      privacyNodes: {type:Array, notify:true, value:config.privacy_nodes},
      selectedPrivacyNode: {type:String, notify:true, observer:'selectedPrivacyNodeChanged'},
      selectedTargetNodes: {type:Object, value:{}},
      selectedTargetNodesArray: {type:Array, computed:'getSelectedTargetNodesArray(selectedTargetNodes)'},
      privateUrlNode: {type: String, value:config.privacy_node_url},
      localAccounts: {type: Array, value:config.local_accounts},
      counter: {type: Number, value:0},
      privateBlockNumber: {type: String, observer:'privateBlockNumberChanged'},
      privateBlockNumberDisplay: {type:String, computed:'getPrivateBlockNumberDisplay(privateBlockNumber)'},
      privateTx: {type: Object},
      privateTxProof: {type: Object},
      dialogParam: String,
      selectedLocalAccount: {type:String},
      isMetamaskSelected: {type:Boolean, computed:'getIsMetamaskSelected(selectedLocalAccount)'},
      usedLocalAccount: {type:String, computed:'getUsedLocalAccount(selectedLocalAccount)', notify:true},
      miningStatus: {type:Object, value:{waiting:false}, notify:true},
      miningMessage: {type:String, computed:'getMiningMessage(miningStatus)'},
      miningMessage2: {type:String, computed:'getMiningMessage2(miningStatus)'},
      
      keyValueStoreAddress: {type:String, value:'', notify:true},
      shouldDisplayCreateKeyValueStore: {type: Boolean, computed:'_shouldDisplayCreateKeyValueStore(keyValueStoreAddress)'},
      shouldDisplayKeyValueStoreAddressInput: {type: Boolean, computed:'_shouldDisplayKeyValueStoreAddressInput(miningStatus)'},
      shouldDisplayKeyValueStoreActions: {type: Boolean, computed:'_shouldDisplayKeyValueStoreActions(keyValueStoreAddress)'},
      shouldDisplaySetKeyValueInput: {type: Boolean, computed:'_shouldDisplaySetKeyValueInput(miningStatus)'},
      keyName: String,
      keyValue: String,
      keyPairLastEvent: {type: Object, notify:true},
    };
  }
  constructor() {
    super()
  }

  selectedPrivacyNodeChanged(selected) {
    // force an update of the list
    let nodes = this.privacyNodes
    this.privacyNodes = []
    nodes.forEach(e=>this.privacyNodes.push( Object.assign({},e) ) )

    let item = config.privacy_nodes.find(e=>e.name==selected)
    if(item) { // in this order
      globalThis.web3js=null
      this.privateUrlNode = item.url
    }

    config.privacy_nodes.forEach(e=>this.selectedTargetNodes[e.name]={checked:true})
    this.selectedTargetNodes = Object.assign({}, this.selectedTargetNodes)
    this.$.privateTxList.loadAllTransactions(true)
  }

  _isSelected(item) {
    //console.log('Compare Nodes', item, this.selectedPrivacyNode)
    return item.name==this.selectedPrivacyNode
  }

  nodeSelectionChanged(e) {
    console.log(e.target)
    if(!e.target.id) return
    this.selectedTargetNodes[e.target.id]={checked:!!e.target.checked}
    this.selectedTargetNodes = Object.assign({}, this.selectedTargetNodes)
    //console.log('selectedTargets:', this.selectedTargetNodes)
  }

  getSelectedTargetNodesArray(selectedTargetNodes){
    return Object.keys(selectedTargetNodes).filter(name=>selectedTargetNodes[name].checked)
  }
  _NewEventKeyValue(event) {
    //console.log('_NewEventKeyValue', event)
    this.keyPairLastEvent = event.detail
  }

  getMiningMessage(miningStatus) {
    if(!miningStatus.detail) return ''
    if(miningStatus.detail.action!='new-contract') return ''
    if(miningStatus.waiting) return 'Creating smart contract in progress: '+'. '.repeat(miningStatus.counting)
    else if(miningStatus.detail.response.address) return ''
    else return 'Some error occurred'
  }
  getMiningMessage2(miningStatus) {
    if(!miningStatus.detail) return ''
    if(miningStatus.detail.action!='set-keypair') return ''
    if(miningStatus.waiting) return 'Recording Key Value: '+'. '.repeat(miningStatus.counting)
    else if(miningStatus.detail.response.status==1) return ''
    else return 'An error occured:'+miningStatus.detail.response.exception
  }

  startMining(detail) {
    this.miningStatus = {detail: detail, waiting:true, counting:0}
    let self = this
    this.miningStatus.interval = setInterval( ()=>{
      let status = self.miningStatus
      status.counting++
      self.miningStatus = Object.assign( {}, status) // must do this to trigger the miningMessage property recalc
    }, 200)
  }
  stopMining(detail) {
    if(this.miningStatus.waiting) {
      if(this.miningStatus.interval) clearInterval(this.miningStatus.interval)
      let status = this.miningStatus
      status.waiting=false
      status.detail=detail
      status.counting=0
      delete this.miningStatus.interval 
      this.miningStatus = Object.assign({}, status)
    }
  }

  blockChanged() {
    this.counter++
  }
  openBlockByNumberDialog(e) {
    this.$.web3EthGetBlockByNumber.triggerRequest()
    this.$.privateBlockDialog.open()
  }
  openSmartContractCodeDialog(e) {
    this.$.smartContractCode.open()
  }

  openPrivateTxDetails(e) {
    if(!e.selected || !e.selected.hash) return
    //console.log("click on hash", e.selected.hash)
    this.privateTx = Object.assign({}, e.selected)
    this.$.privateTxDialog.open()
  }

  showTxProof(e) {
    console.log('showTxProof', this.privateTx.hash)
    this.$.rpcGetTxProof.params=[this.privateTx.hash]
    this.$.rpcGetTxProof.triggerRequest()
  }
  onRpcTxProofResponse(e) {
    console.log('onRpcTxProofResponse', e)
    this.privateTxProof = e.detail
    this.$.privateTxProofDialog.open()
  }

  createSmartContract() {
    console.log("Account selected ", this.$.accountSelector.value)
    this.$.btCreateSmartContract.setAttribute('disabled', true)
    this.$.keyValueStore.createNewInstance() // TO BE CORRECTED
    //if( this.keyValueStoreAddress=="") this.keyValueStoreAddress='0xf665204028d5fffd7eb8116635ec1673dc880532'
    //else this.keyValueStoreAddress=""
    //this.miningStatus = {detail:{action:'new-contract'}, waiting:true}
  }
  kvsStartTx(e) {
    console.log('kvsStartTx', e)
    this.startMining(e.detail)
  }
  kvsEndTx(e) {
    console.log('kvsEndTx', e)
    this.stopMining(e.detail)
    if(e.detail.action=='new-contract')
      this.$.btCreateSmartContract.removeAttribute('disabled')
    if(e.detail.action=='set-keypair')
      this.$.btSetKeyPair.removeAttribute('disabled')
    this.$.privateTxList.loadAllTransactions()
  }

  privateBlockNumberChanged() {
    this.$.privateTxList.loadAllTransactions()
  }

  _shouldDisplayKeyValueStoreActions(keyValueStoreAddress) {
    return !!keyValueStoreAddress && keyValueStoreAddress.match(/^0x[0-9a-fA-F]{40}$/)
  }
  _shouldDisplayCreateKeyValueStore(keyValueStoreAddress) {
    return !this._shouldDisplayKeyValueStoreActions(keyValueStoreAddress)
  }
  _shouldDisplayKeyValueStoreAddressInput(miningStatus) {
    if(!miningStatus.detail) return true
    if(miningStatus.detail.action!='new-contract') return true
    if(miningStatus.waiting) return false
    return true
  }
  _shouldDisplaySetKeyValueInput(miningStatus) {
    if(!miningStatus.detail) return true
    if(miningStatus.detail.action!='set-keypair') return true
    if(miningStatus.waiting) return false
    return true
  }

  setKeyPair() {
    //console.log('SetKeyPair', this.keyName, this.keyValue)
    if(this.keyName.length==0) return 
    this.$.btSetKeyPair.setAttribute('disabled', true)
    this.$.keyValueStore.setKeyValue(this.keyName, this.keyValue)
  }

  getIsMetamaskSelected(selectedLocalAccount) {
    return selectedLocalAccount=='metamask'
  }
 
  getUsedLocalAccount(selectedLocalAccount) {
    if(selectedLocalAccount=='metamask') {
      if(globalThis.ethereum && globalThis.ethereum.isMetaMask ) {
        if(+globalThis.ethereum.networkVersion==globalThis.config.private_netword_id) {
          if(globalThis.ethereum.selectedAddress)
            return globalThis.ethereum.selectedAddress
          else return 'Login to metamask and select an account'
        } else return 'Configure metamask to connect to '+this.privateUrlNode+' . Beware, if you change the node change you metamask configuration as well!'
      } else return 'Install metamask first in your browser'
    } else return selectedLocalAccount
  }

  getPrivateBlockNumberDisplay(privateBlockNumber) {
    if(privateBlockNumber)
      if(privateBlockNumber.startsWith('0x')) return +privateBlockNumber
      else return privateBlockNumber
    else return 'Invalid value'
  }

  ready() {
    super.ready()
    let self = this

    function forceSelectedAccountUpdate() {
      let val = self.selectedLocalAccount
      self.selectedLocalAccount=""
      self.selectedLocalAccount=val // force updating the selection
    }
    if(globalThis.ethereum && globalThis.ethereum.isMetaMask ) {
      globalThis.ethereum.autoRefreshOnNetworkChange = false
      globalThis.ethereum.on('accountsChanged', forceSelectedAccountUpdate)
      globalThis.ethereum.on('networkChanged', forceSelectedAccountUpdate)
      globalThis.ethereum.enable().then(accounts=>console.log('Metamask accounts:', accounts))
    }

    console.log('Find web3 instance:', web3Instance())

    if(config.default_keyvalue_smart_contract) // for tests
      this.keyValueStoreAddress = config.default_keyvalue_smart_contract
    
    // BUG TO BE INVESTIGATED: when in dom-if an object is not in this.$. WORKAROUND HERE
    // force adding them !!
    /*Object.defineProperty(this, '$', {
      enumerable:true,
      get: function() { 
        let o={}; 
        this.shadowRoot.querySelectorAll('*[id]').forEach(e=>o[e.id]=e) 
        return o
      }
    })*/
    this.$ = new Proxy(this.shadowRoot, {
      get: function(obj, prop){
        return obj.querySelector('#'+prop)
      },
      has: function(obj, prop) {
        return obj.querySelector('#'+prop)!=null
      },
      ownKeys: function(obj) {
        return obj.querySelector('*[id]').map(e=>e.id)
      }
    })
  }
  
}
window.addEventListener('WebComponentsReady', function() {
  declareComponent(Content)
})