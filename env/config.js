// This is the development config by default 
// for deployment create a separate config file and deploy it under config.js name

// this file attach a config js object under the globalThis global variable

globalThis.config = {
    etherscan_root: 'https://ropsten.etherscan.io',
    etherscan_api_root: 'https://api-ropsten.etherscan.io',
    privacy_node_url: 'https://qualif.docchain-api.ca-cib.com/privacy1',
    private_tx_smart_contract: '0xd5d36550a1d7971b4fa68be066ed4ef4f85a46e1',
    private_netword_id:6000,
    privacy_nodes: [
        {
            name: 'node A',
            url: 'https://qualif.docchain-api.ca-cib.com/privacy1',
        },
        {
            name: 'node B',
            url: 'https://qualif.docchain-api.ca-cib.com/privacy2',
        },
        {
            name: 'node C',
            url: 'https://qualif.docchain-api.ca-cib.com/privacy3',
        }
    ],
    local_accounts: ["0x7a020b20e76eeb2af986fe525c822ac3557ba1ed", "0xf665204028d5fffd7eb8116635ec1673dc880532"],
    my_donation_account: "0x1a1a265c772bbfa736013a62e06172e153502673",
}